const express = require('express')
const port = parseInt(process.env.PORT, 10) || 3000

require('dotenv').config();
const internetAvailable = require("internet-available");

const path = require('path')
const {spawn} = require('child_process')
const fs = require('fs')


const OmxManager = require('omx-manager');
const manager = new OmxManager(); // OmxManager

// Require child_process
const exec = require('child_process').exec;

const Wifi = require('./modules/wifi/wifi')
const wifi = new Wifi()

// Module gérant les téléchargements
const DownloadManager = require('./modules/downloadManager');
const downloadManager = new DownloadManager();

const Config = require('./modules/config')
const config = new Config()

const Sequence = require('./modules/sequence')
const sequence = new Sequence()

const Settings = require("./modules/settings")
const settings = new Settings()


let serverApp, player, online, interf, mac_address, settingsBox, seq, _currentSpeedBps = 0, initData, displayMac, displayImage, connectedOnServer = false, Loader, reset = false, Downloading, selectedFile, statusPlayer = false

const io = require("socket.io")(serverApp)
const socketClient = require("socket.io-client")

    const server = express();

    const testConnection = async() => {
        try{
            await internetAvailable({
                timeout: 15000,
                retries: 15
            })
            return true
        } catch(err){
            return false
        }
    }

    const init = async () => {
        console.log('--------------------- init -------------------------')
        try{
            let initConfig = await Promise.all([wifi.getMacAddress(), settings.read(), sequence.read(), config.read() ]);
            [mac_address, settingsBox, seq ] = initConfig
            console.log(`Mac : ${mac_address}`)

            await wifi.disable()
            console.log('wifi disabled ! INIT')
            online = await testConnection()
            console.log("ONLINE***********************", online)
            return({ online : online, mac: mac_address, settings : settingsBox, sequence: seq })
        } catch(e){
            console.log(e)
        }
    }

    const getHotspotInfo = async() => {
        console.log('get hotspot info')
        await wifi.generateSSID()
        /*await wifi.disable()
        await wifi.enable()*/
        const resume = await wifi.resume()
        return resume
    }

    /***********IMPORTANT*********************/
    //HDMI-1 à changer en fonction du device
    //lancer commande xrandr
    const rotateScreen = position => {
        const positions = {
            0: () => exec("xrandr --output HDMI-1 --rotate normal", { shell: '/bin/bash' }),
            90: () => exec("xrandr --output HDMI-1 --rotate right", { shell: '/bin/bash' }),
            180: () => exec("xrandr --output HDMI-1 --rotate inverted", { shell: '/bin/bash' }),
            270: () => exec("xrandr --output HDMI-1 --rotate left", { shell: '/bin/bash' })
        }
        console.log('rotate', position)
        positions[position]();
    }

    server.get('*', (req, res) => handle(req, res));

    /*-----------------------------RUN SERVER-----------------------------*/
    serverApp = server.listen(port, async (err) => {
        if (err) throw err;
        console.log("settings.rotation", settings.rotation)
        await settings.read()
        rotateScreen(settings.rotation)
        startApp()

    });

    function filesSequence(){
        this.currentSlotIndex = 0
        this.play = function(slots){
            reset = false
            if(slots.length > 0){
                selectedFile = slots[this.currentSlotIndex]["template"].template_fields[0]
                if(selectedFile.slug == "video"){
                    //console.log("selectedFile", selectedFile)
                    let timer
                    player = manager.create(`${__dirname}/assets/${selectedFile.filename}`, { '--orientation': settings.rotation, "--win" : "0 0 640 480"})
                    player.on('play', () => {
                        statusPlayer = true
                        console.log("Play")
                        //console.log("DURATION", slots[this.currentSlotIndex].duration)
                        timer = setTimeout(() => {
                            console.log("Timeout")
                            this.stop()
                        }, slots[this.currentSlotIndex].duration + 10000)
                    })
                    player.on('end', () => {
                        console.log('END PLAYER')
                        clearTimeout(timer)
                        if(!reset){
                            console.log('stopped')
                            this.nextFile()
                        }
                    });
                    player.play()
                } else if(selectedFile.slug == "image"){
                    console.log(selectedFile)
                    displayImage = spawn('python3', [
                        "-u",
                        path.join(__dirname, 'main.py'),"-i",
                        `${__dirname}/assets/${selectedFile.filename}`, settings.rotation
                    ]);
                    setTimeout(() => {
                        displayImage.kill()
                        this.nextFile()
                    }, slots[this.currentSlotIndex].duration)
                } else if(selectedFile.slug == "__render" && selectedFile.value.slice(selectedFile.value.lastIndexOf('/') + 1
                ,selectedFile.value.lastIndexOf('?decache'))){
                    selectedFile.filename = selectedFile.value.slice(selectedFile.value.lastIndexOf('/') + 1
                    ,selectedFile.value.lastIndexOf('?decache'))
                    player = manager.create(`${__dirname}/assets/${selectedFile.filename}`, { '--orientation': settings.rotation})
                    player.on('play', () => {
                        statusPlayer = true
                        console.log("Play")
                    })
                    player.on('end', () => {
                        console.log('END PLAYER')
                        if(!reset){
                            console.log('stopped')
                            this.nextFile()
                        }
                    });
                    player.play()

                } else if(selectedFile.slug == 'audio'){
                    player = manager.create(`${__dirname}/assets/${selectedFile.filename}`, { '--orientation': settings.rotation})
                    player.on('play', () => {
                        statusPlayer = true
                        console.log("Play")
                    })
                    player.on('end', () => {
                        console.log('END PLAYER')
                        if(!reset){
                            console.log('stopped')
                            this.nextFile()
                        }
                    });
                    player.play()
                } else {
                        console.log('next')
                        this.nextFile()
                }
            }
        }
        this.nextFile = function(){
            this.currentSlotIndex++
            if (this.currentSlotIndex > initData.sequence.slots.length -1 ){ this.currentSlotIndex = 0 }
            selectedFile = initData.sequence.slots[this.currentSlotIndex]["template"].template_fields[0]
            this.play(initData.sequence.slots)
        }

        this.stop = function(){
            player.stop()
        }
    }

    let customPlayer = new filesSequence()

    const startApp = async () => {

        const delay = (ms) => {
            return new Promise(resolve => setTimeout(resolve, ms))
        }
    
        const tryToConnect = async() => {
            try{
                console.log("************************TRY TO CONNECT*****************************")
                let connected = await testConnection()
                let inter;
                //if(fs.existsSync("/etc/wpa_supplicant/wpa_supplicant-wlan0.conf")){
                    if(!connected){
                        inter && clearTimeout(inter)
                        await wifi.disable()
                        await wifi.enable()
                        await delay(180000)
                        console.log("End delay")
                        await wifi.disable()
                    } else if(connected && !connectedOnServer){
                        await startRemotePortal()
                            startRemoteServer()
                        //clearTimeout(inter)
                    }
                    inter = setTimeout(tryToConnect, 60000)
                // } else {
                //     await wifi.disable()
                //     await wifi.enable()
                // }
            } catch(err){
                console.log(err)
            }
        }

        //initialize setting Box
        console.log("Loader start")
        Loader = spawn('python3', ["-u",
            path.join(__dirname, 'main.py'),"-l", settings.rotation
        ]);
        initData = await init()
        hotspotData = await getHotspotInfo()
        if(!initData.online){
            Loader.kill()
            if(initData.sequence.id){
                //Play videos
                customPlayer.play(initData.sequence.slots)
            } else {
                console.log("***************KILL LOADER********************")
                //Loader.kill()
                console.log('Display host')
                displayHost = spawn('python3', ["-u", path.join(__dirname, 'main.py'), "-h", hotspotData.ssid , hotspotData.passphrase]);
                //await wifi.enable()
            }
        } else {
            if(!initData.sequence.id){
                Loader.kill()
                displayMac = spawn('python3', ["-u", path.join(__dirname, 'main.py'), "-m", mac_address, settings.rotation]);
                await startRemotePortal()
                startRemoteServer()
            } else {
                Loader.kill()
                //Play videos
                console.log('initData.sequence.id')
                customPlayer.play(initData.sequence.slots)
                await startRemotePortal()
                startRemoteServer()
            }
        }
        tryToConnect()

        const saveData = {
            saveSequence : async (data) => {
                try{
                    await sequence.save(data)
                    await downloadAssets();
                }
                catch(e) {
                    console.log("LOCAL: Impossible de sauvegarder la séquence");
                    console.log(e);
                };
            },
            saveSettings : async (data) => {
                try{
                    await settings.save(data)
                    console.log("LOCAL: Settings sauvegardées");
                }
                catch(e) {
                    console.log("LOCAL: Impossible de sauvegarder les settings");
                };
            }
        }


    /*-----------------------------SOCKET-----------------------------*/


    io.on('connect', (socket) => {
        console.log('socket connected !')
    })

    function startRemotePortal() {
        return new Promise((resolve, reject) => {
            // Connexion au portail
            console.log("Connexion au portail: " + config.portal);
            let clientPortal = socketClient(config.portal);
            // Connexion établie
            clientPortal.on('connect', function() {
                console.log("Connecté au portal");
                console.log("Envoi de la Mac Address");

                clientPortal.emit('mac_address:send', {
                    mac_address: mac_address
                });

                clientPortal.on('server:send', async (server) => {
                    try{
                        await config.save({ server: server })
                        // Server enregistré, on valide
                        console.log(`Serveur reçu: ${server}`);
                        resolve();
                    } catch(e){
                            // échec d'enregistrement, on rejecte et on kill le socket
                            reject(err);
                    };
                });
            });
        });
    }


    function shutdown(callback) {
        exec('reboot', function(error, stdout, stderr) {
            callback(stdout);
        });
    }

    function startRemoteServer() {
        let connectionAttempt = 0;
        // On attend une seconde avant de se connecter au serveur, pour que le SQL soit à jour
        setTimeout(
            function() {
                // Connexion
                console.log("Connexion: " + config.server);
                // Socket client
                const clientServer = socketClient(config.server);

                // Event - Connecté au serveur
                clientServer.on('connect', function() {
                    console.log("Connecté");
                    console.log("Login ...");
                    clientServer.emit('login', {
                        mac_address: mac_address
                    });
                });
                // Event - Reconnecté au serveur
                clientServer.on('reconnect', function() {
                    console.log("Reconnecté");
                    //connectedOnServer = true
                });
                // Event - Erreur de connexion au serveur
                clientServer.on('connect_error', function() {
                    console.log("Erreur connexion au serveur Node");
                    connectionAttempt++;
                    // Si on a plus de 10 échecs de connexion au serveur, on déconnecte, on supprime le serveur de la config et on reconnecte au portal
                    if (connectionAttempt >= 10) {
                        console.log("10 échecs de connexion au serveur, déconnexion et requête au portail");
                        clientServer.disconnect();
                        // Connexion au portal
                        startRemotePortal()
                            .then(() => {
                                startRemoteServer();
                            });
                    }
                });

                // Event - Erreur
                clientServer.on('error', function(data) {
                    console.log("Erreur");
                    console.log(data);
                });

                // Event - Logged
                clientServer.on('logged', function() {
                    console.log("Loggé");
                    connectedOnServer = true
                    try{
                        displayMac.kill()
                    } catch(e){console.log("Already register")}
                    _serverSocket = clientServer;

                    if (_serverSocket.connected && typeof _commit !== "undefined") {
                        _serverSocket.emit("debug:commit", { commit: _commit });
                    }
                });

                // Event - Déconnecté
                clientServer.on('disconnect', function() {
                    console.log("Déconnecté du serveur");
                    //clientServer.disconnect();
                    connectedOnServer = false
                });

                // Event - Sequence envoyée par le serveur
                clientServer.on('sequence:send', function(data) {
                    console.log("Sequence reçue");
                    saveData.saveSequence(data);
                });

                // Event - Reboot du device demandée
                clientServer.on('device:reboot', function() {
                    console.log("Reboot en cours ...");
                    // Reboot computer
                    shutdown(function(output) {
                        console.log("Reboot requested");
                    });
                });

                // Event - Reboot du device demandée
                clientServer.on('device:reset', function() {
                    console.log("Reset en cours ...");
                    // - On arrête la vidéo s'il y en a une en cours
                    reset = true
                    try {
                        customPlayer.stop();
                    } catch (err) {
                        console.log("Video not playing");
                    }
                    /*Loader = spawn('python3', ["-u",
                        path.join(__dirname, 'main.py'),"-l",
                    ]);*/
                    setTimeout(() => {
                        // - écran de loading
                        //_localSocket.emit('redirect', 'loading');

                        // Commandes de reset
                        let cmdCd = `cd ${__dirname}`;
                        let cmdRmConfig = `rm -f data/config.json`;
                        let cmdRmRotation = `rm -f data/rotation`;
                        let cmdRmSettings = `rm -f data/settings.json`;
                        let cmdRmSequence = `rm -f data/sequence.json`;
                        let cmdRmAssets = `rm -f assets/*`;
                        let cmdRmWpa = `rm -f /etc/wpa_supplicant/wpa_supplicant-wlan0.conf`;
                        let cmdCpConfig = `cp data/samples/config.json data/`;
                        let cmdCpSettings = `cp data/samples/settings.json data/`;
                        let cmdCpSeq = `cp data/samples/sequence.json data/`;
                        let cmdRmDhcp = `rm -f /var/lib/dhcpd/*.lease`;
                        let cmdRmDnsmasq = `rm -f /var/lib/misc/dnsmasq.leases`;

                        // - reset
                        exec(
                            `${cmdCd} && ${cmdRmConfig} && ${cmdRmWpa} && ${cmdRmRotation} && ${cmdRmSettings} && ${cmdRmSequence} && ${cmdRmAssets} && ${cmdCpSeq} && ${cmdCpConfig} && ${cmdCpSettings} && ${cmdRmDhcp} && ${cmdRmDnsmasq}`, { shell: '/bin/bash' },
                            (errReset) => {
                                if (!errReset) {
                                    // - emit
                                    console.log('Reset effectué');
                                    clientServer.emit('device:reseted');
                                } else {
                                    console.log('Reset échoué');
                                    console.log(errReset);
                                    clientServer.emit('reset:failed');
                                }
                            });
                    }, 5000);
                })

                // Event - Settings envoyées par le serveur
                clientServer.on('settings:send', function(data) {
                    console.log("Settings reçues");
                    if(data.rotation != settings.rotation){
                        rotateScreen(data.rotation)
                    }
                    saveData.saveSettings(data);
                    // set the timezone of the box
                    exec('timedatectl set-timezone ' + data.timezone);
                });
        })
    }

// ----------------------------------------------------------------------------
// Downloader
// ----------------------------------------------------------------------------
function progress(state) {
    //Calcul de la vitesse de téléchargements à afficher (en b, Kb ou Mb /s)
    let speed;
    if (state.speed > 1024) {
        if (state.speed > 1048576) {
            speed = `${(state.speed/1048576).toFixed(2)}Mb/s`;
        } else {
            speed = `${(state.speed/1024).toFixed(2)}Kb/s`;
        }
    } else {
        speed = state.speed + "b/s";
    }
    state.formattedSpeed = speed;
    // Envoi de l'event "state download" au serveur
    if (_serverSocket.connected && state.speed != _currentSpeedBps) {
        _currentSpeedBps = state.speed;
        _serverSocket.emit("download:state", { speed: _currentSpeedBps });
    }
}

    async function downloadAssets() {
        console.log("Starting asset download");
        const assets = await sequence.getAssets()
            if (assets.length <= 0) {
                console.log("Aucun fichier à télécharger!");
                if (sequence.slots) {
                    console.log('Envoi des slots');
                    //Emit slot
                    //_localSocket.emit('slots:send', sequence.slots);
                    //_localSocket.emit('redirect', 'sequence');
                } else {
                    //Redirect Empty slot
                    //_localSocket.emit('redirect', 'empty');
                }
                // Envoi de l'event "downloaded" au serveur
                if (_serverSocket.connected) {
                    _serverSocket.emit("assets:downloaded", true);
                }
            } else {
                if(!statusPlayer){
             	    //Downloading = spawn('python3', ["-d", path.join(__dirname, 'main.py'),"-d", settings.rotation ]);
                }}
                // Téléchargements des fichiers
                try{
                    await downloadManager.download(assets, 'assets/', progress, "https://inovhub.s3.eu-west-3.amazonaws.com/speed.test" /*sequence.speedFile*/ )
                        console.log("Fichier(s) téléchargé(s)");
                        // Envoi de l'event "downloaded" au serveur
                        if (_serverSocket.connected) {
                            _serverSocket.emit("assets:downloaded", true);
                        }

                        if (sequence.slots) {
                            //await sequence.read()
                            initData = await init()
                        if(!statusPlayer){
                            Loader.kill();
                            //Downloading.kill()
                            console.log('Here we go again !!')
                            //player = customPlayer.settingsPlayer(settings.rotation)
                            customPlayer.play(initData.sequence.slots)
                        } else {
                            try{
                            //Loader.kill();
                            //Downloading.kill();
                            } catch(e){console.log('Loader already kill')}
                        }
                        } else {
                            //_localSocket.emit('redirect', 'empty');
                        }
                }
                catch(err) {
                    console.log(err);
                    console.log("Impossible de télécharger le(s) fichier(s)");
                };
    }
    
}
